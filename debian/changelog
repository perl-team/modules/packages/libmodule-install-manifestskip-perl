libmodule-install-manifestskip-perl (0.24-3) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old via debian/clean. (Closes: #1048919)

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 14:57:41 +0100

libmodule-install-manifestskip-perl (0.24-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jenkins ]
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 20:02:51 +0100

libmodule-install-manifestskip-perl (0.24-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 13:21:50 +0100

libmodule-install-manifestskip-perl (0.24-1) unstable; urgency=low

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to use canonical hostname (anonscm.debian.org).
  * Update Vcs-Browser URL to cgit web frontend.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Bump debhelper compatibility to 8: Needed package version is
    satisfied even in oldstable.
  * Bump compatibility claim to Policy 3.9.5.
  * Fix Vcs-Git to use canonical URL.
  * Update copyright info:
    + Extend coverage of packaging, and bump its licensing to GPL-3+.
    + Fix use license and comment pseudo-sections to obey silly
      restrictions of copyright format 1.0.
    + Add alternate git source URL.
    + Drop Files sections for no longer shipped convenience code copies.
    + Use upstream bugtracker and IRC channel as preferred upstream
      contact.
    + Extend coverage for upstream author.
  * Relax to build-depend unversioned on cdbs: Needed version satisfied
    even in oldstable.
  * Update watch file to use metacpan.org and cpan.org/modules URLs (not
    search.cpan.org URL).
  * Stop track or mangle development releases.
  * Use github URL as Homepage, to reflect upstream hinting.
  * Preserve some upstream shipped cruft from being cleaned.
  * Stop track checksum of upstream tarballs.
  * Update package relations:
    + (Build-)depend on recent libmodule-install-perl.
    + Tighten (build-)dependency on libmodule-manifest-skip-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Sep 2014 13:20:48 +0200

libmodule-install-manifestskip-perl (0.20-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#675770.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 03 Jun 2012 12:55:39 +0200
